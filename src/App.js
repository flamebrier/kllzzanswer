/*import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';*/


const fs = require('fs');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const PROTO_PATH = 'api.proto';
//const serviceDef = grpc.load(PROTO_PATH);
const serviceDef = protoLoader.loadSync(PROTO_PATH);
const apiProto = grpc.loadPackageDefinition(serviceDef).api;

const cacert = fs.readFileSync('Keys/ca.crt');
const cert = fs.readFileSync('Keys/client.crt');
const key = fs.readFileSync('Keys/client.key');
const kvpair = {
    'private_key': key,
    'cert_chain': cert
};

const credentials = grpc.credentials.createSsl(cacert);
const client = new apiProto.Metrics('apitest.kllzz.ru:443', credentials);
console.log(client);

function getStats(client) {
    const call = client.getStats({});

    call.on('data', function (data) {
        console.log(data);
    });
}

getStats(client);

/*class App extends Component{
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <PulseDisplay zip={"12345"}/>
                </header>
            </div>
        );
    }
}

export default App;*/